﻿## Python & Selenium



### Python jako język programowania

* wysokiego poziomu
* wspierający paradygmaty
  * imperatywny (proceduralny)
  * obiektowy
  * funkcyjny
* interpretowalny
* skryptowy
* dynamiczny
* z dynamicznym typowaniem
* wszystko jest obiektem



### Interactive shell && Python IDLE



#### Typy obiektów

| Typy         | Przykład                                          |
| ------------ | ------------------------------------------------- |
| Numbers      | 1234, 3.1415, 999L, 3+4j, Decimal                 |
| Strings      | 'spam'                                            |
| Lists        | [1, [2, 'three'], 4]                              |
| Dictionaries | {'food': 'spam', 'taste': 'yum'}                  |
| Tuples       | (1,'spam', 4, 'U')                                |
| Sets         | set(), {'one', 'two', 'three'}                    |
| Other types  | Function, modules, classes, Booleans, None, types |



### Zmienna



### Numbers

```python
var = 3
var = 5 + 9
```


### Operatory artymetyczne

| Operator | Opis                                | Przykład   |
| -------- | ----------------------------------- | ---------- |
| \+       | Dodawanie liczb                     | a = 4 + 5  |
| \-       | Odejmowanie liczb                   | a = 6 - 5  |
| \*       | Mnożenie liczb                      | a = 3 * 4  |
| /        | Dzielenie liczb                     | a = 4 / 2  |
| %        | Modulo reszta z dzielenia           | a = 5 % 2  |
| //       | Floor division dzielenie bez reszty | a = 5 // 2 |
| **       | Podniesienie do potęgi              | a = 3 ** 2 |



### Operatory lewostronne ( assignment operators)

| Operator | Opis                                                                                  | Przykład |
| -------- | ------------------------------------------------------------------------------------- | -------- |
| =        | Przypisuje wartość liczby po prawej strony do operanda po lewej                       | a = 20   |
| +=       | Dodawanie operanda po prawej stronie do operanda po lewej                             | a += 1   |
| -=       | Odejmowanie operanda po prawej stronie od operanda po lewej                           | a -= 4   |


| Operator | Opis                                                                                  | Przykład |
| -------- | ------------------------------------------------------------------------------------- | -------- |
| *=       | Mnożenie operanda po prawej stronie przez operanda po lewej                           | a *= 5   |
| /=       | Dzielenie operanda po prawej stronie do operanda po lewej                             | a /= 7   |
| %=       | Modulo reszta z dzielenia operanda po prawej stronie przed operanda po lewej          | a %= 2   |
| //=      | Floor division dzielenie bez reszty operanda po prawej stronie przez operand po lewej | a //= 2  |



| Operator | Opis                                                                                  | Przykład |
| -------- | ------------------------------------------------------------------------------------- | -------- |
| **=      | Podniesienie do potegi operanda po lewej stronie przez operand po prawej              | a **= 2  |



### String

* immutable

```python
var = 'string'
var += ' drugi'
var = 3
string = 'replaceMe'
print(string.replace('Me', ''))
print(string)
```


### Print function

`print(...)
    print(value, ..., sep=' ', end='\n', file=sys.stdout, flush=False)`


### String formatting

* Stary sposób - %

```python
string_to_print = '%s %s' % ('one', 'two')

print(string_to_print)
```

* Nowy sposób - {} i metoda formatująca

```python
string_to_print = '{} {}'.format('one', 'two')

print(string_to_print)
```



### Funkcje

* funkcja nie zwracająca wartości

```python
def mul(number, number2):
    print(number * number2)
```

* funkcja zwracająca wartość _return_

```python
def mul_return(number, number2):
    print("Inside of function with values {} {}".format(number, number2))
    return number * number2
```



### Wcięcia kodu

![Wcięcia](img/blocks.png)



### Komentarze

* komentarz zaczyna się od _#_

```python
# comment
def mul(number, number2):
    return number * number2  
    # Returning number multiplied by number2
```



### Docstring

* docstring to linie pomiędzy _""" """_ lub _''' '''_

```python
def mul(number, number2):
    """
    Function returning number multiplied by number2
    :param number: Number
    :param number2: Number
    :return: Number
    """
    return number * number2  # Returning number
                             # multiplied by number2

print(mul.__doc__)
print(help(mul))  # Show help
```



### Anotations

```python
def mul(number: float, number2: float) -> float:
    return number * number2

print(mul(2, 3))
```

```python
def mul(number: float, number2: float) -> None:
    print(number * number2)

mul(2, 3)
```



### Zadania



### Zlożone struktury danych: list

* ordered sequence

```python
empty_list = []
list_a = [1, 2, 4, 5, 6]
print(list_a[2])

list_a.remove(2)
print(list_a)

print(list_a.pop())
print(list_a)

list_a[2] = 10
print(list_a)

list_a.append(1)
print(list_a)
```



### Zlożone struktury danych: tuple

* immutable ordered sequence

```python
empty_tuple = ()
tuple_b = (1, 2, 4, 5, 6)
print(tuple_b)

print(tuple_b[2])

# tuple_b.remove(2)  # Error

# tuple_b[1] = 2  # Error

# print(tuple_b.pop())  # Error

# tuple_b.append(1)  # Error
```



#### Indexowanie & Cięcie (slice)

* Działaja dla wszystkich sekwencji: str, list, tuple

![Indexing & Slicing](img/slice.png)



### Operatory zawierania

* Działają dla wszystkich sekwencji: str, list, tuple

| Operator | Opis                                  | Przykład          |
| -------- | ------------------------------------- | ----------------- |
| in       | True gdy zmienna jest w sekwencji     | 'a' in list_a     |
| not in   | True gdy zmienna nie jest w sekwencji | 'b' not in list_a |


### Zlożone struktury danych: dictionary

* key / value association

```python
empty_dict = {}
dict_c = {'a': 1, 'b': var, 'c': "dfg", 'd': '345', 'e': 6}
print(dict_c['b'])

print(dict_c.pop('c'))
print(dict_c)

dict_c['d'] = 2
print(dict_c)

dict_c.update({'e': 7, 'g': 9, 'h': 11})

print(dict_c)
```


### Zlożone struktury danych: set

* Unordered group of unique elements

```python
empty_set = set()
set_d = {1, 2, 3, 'second_text', 4, 5, 6, 7, 'txt'}
set_e = {8, 9, 10, 11, 'my_string', 12, 13}

set_d.add(1)
set_d.add(23)
print(set_d)

set_d.remove(6)
print(set_d)

set_d.update({5, 6, 7, 8, 9})
print(set_d)
```



```python
set_f = set_e.union(set_d)
print(set_f)
print(set_d.intersection(set_e))

sub_set = {1, 2, 3}
print(sub_set.issubset(set_d))
print(set_d.issuperset(sub_set))
```



### Konstrukcja warunkowa if

```python
lista_a = [1, 2, 3, 4, 5, 6]

if a in lista_a:
    print('{} is in list'.format(a))
elif b in lista_a:
    print('{} is in list'.format(b))
else:
    print('Not in list')
```


### Operatory porównania

| Operator | Opis                                               | Przykład |
| -------- | -------------------------------------------------- | -------- |
| >        | True gdy lewy operand większy od prawego           | a > b    |
| <        | True gdy prawy operand większy od lewego           | a < b    |
| ==       | True gdy lewy i prawy operand równe                | a == b   |
| !=       | True gdy lewy i prawy operand różne                | a != b   |



| Operator | Opis                                               | Przykład |
| -------- | -------------------------------------------------- | -------- |
| >=       | True gdy lewy operand większy lub równy od prawego | a >= b   |
| <=       | True gdy prawy operand wi ekszy lub równy o lewego | a <= b   |


### Operatory logiczne

| Operator        | Opis                                    | Przykład |
| --------------- | --------------------------------------- | -------- |
| and Logical AND | True gdy obydwa operandy wartości true  | a and b  |
| or  Logical OR  | True gdy jeden z operandów wartości tru | a or b   |
| not Logical NOT | Zanegowanie wartości logicznej operanda | not a    |



### Konstrukcja sterująca while

```python
a = [1, 'd', 3, '4', '6', 'g']

i = 0
while i < len(a):
    print(a[i])
    i += 1
```


### Konstrukcja sterująca while / else

```python
a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
finding = 20
i = 0
while i < len(a):
    if a[i] == finding:
        print('Found')
        break
    i = i + 1
else:
    print('Not found')
```



### Konstrukcja sterująca for

* W pythonie _for_ to tak naprawdę _for each_ dla kazdego elementu z sekwencji

```python
a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

for i in a:
    print(i)

d = {'a': 1, 'b': 2, 'c': 3, 'd': 4}

for key, value in enumerate(d):
    print('{}: {}'.format(key, value))
```


* Żeby zrobić for

```python
for i in range(0, 10):
    print(i)
```



### Funkcje zawansowane


### Rozpakowywanie list, tuple i dictionary

* Pojedyncza gwiazdka - rozpakowywanie listy, tuple

```python
def unpacking_function(a, b, c, d):
    print(a, b, c, d)

list_a = [4, 5, 6, 7]
list_b = (7, 8, 9, 19)

unpacking_function(*list_a)
unpacking_function(*list_b)
```


* Podwójna gwiazdka - rozpakowywanie dictionary

```python

dict_a = {'a': 8, 'b': 4, 'c': 10, 'd': 11}

unpacking_function(**dict_a)

```



### Zmienne lokalne i globalne

```python
def changing():
    global s
    s = "Changing global variable"

changing()
print(s)
```



### Zadania



### Moduły

* Moduł - część kodu która ma pewną funkcjonalność
* W pythonie moduł to plik z rozszerzeniem *.py Nazwą modułu jest nazwa pliku


### Importowanie modułów

* `import module`

```python
import random

print(random.random())
print(random.triangular(2, 3))
```


* `import module as custom_name`

```python
import random as custom_name

print(custom_name.random())
print(custom_name.triangular(2, 3))
```


* `from module import function, function2` - importowanie obiektów z modułu do obecnej przestrzeni nazw

```python
from random import random, triangular

print(random())
print(triangular(2, 3))
```


* `from module import *` - importowanie wszystkich obiektów z modułu do obecnej przestrzeni nazw

```python
from random import *

print(random())
print(triangular(2, 3))
```



### Underscore _ w Python

* *_underscore*  
  * ostatnia wartość w python shellu
  * ignorowanie wartości przy rozpakowywaniu
  * zmienna prywatna która nie zostanie zaimportowana przez `from module import *`, (weak private)



### Underscore _ w Python

* *\__double_leading_underscore* - python doklei _ClassName na początku atrybut z \__ , a więc \__name zostanie \_ClassName\__name
* *\__double_leading_and_trailing_underscore\__* - oznacza magiczne methody pythona na przykład: \__init\__ dla initializacji obiektu, \__str\__ do wyświetlania stanu obiektu w stringu



### Pakiety

* Pakiet - kilka modułow razem (także podpakietów)
* Aby katalog przez pythona był traktowany jako pakiet w katalogu musi być plik *\__init\__.py*



### Klasa i obiekt

* Klasa - _prototyp_ z którego tworzony jest obiekt
* Obiekt - _unikalna instancja_ struktury danych na podstawie jego klasy


### Podstawowa struktura klasy

```python
class Employee:
    # initialize the attributes
    def __init__(self, name, department, title):
        self.name = name
        self.department = department
        self.title = title

    def work(self):
        print('Working')

if __name__ == "__main__":
    first_employee = Employee('Jan Becker', 'Testing', 'Tester')
    second_employee = Employee('Jan Weber', 'Development', 'Developer')
    print(first_employee.department)
    print(second_employee.name)
```



### Dziedziczenie

```python
class Tester(Employee):  # Inheritance
    def __init__(self, name, title='Tester'):
        super().__init__(name, 'Testing', 
                        title)  # run parent method

    def working(self):
        print('Writing tests')

if __name__ == "__main__":
    first_tester = Tester('Staszek')
    second_tester = Tester('Jasiek', 'Senior tester')
    first_tester.working()
```



### Zadania



### Wyjątki i ich obsługa

```python
a = 3
b = 4
division = 0
try:
    a = a / division
    b = b / division
except Exception as e:
    print(e)
else:
    print('Dzielenie OK')
finally:
    print('Finally\n a: {}\n b: {}\n'.format(a, b))
```



### Praca z plikami tekstowymi

```python
file = open('file.txt', 'w')
file.write('linia')
file.close()

file = open('file.txt', 'r')
print(file.read())
file.close()
```


### Praca z plikami tekstowymi - with

```python
with open('file.txt', 'r') as file:
    print(file.read())
```



### Automatyzacja i jej poziomy



### Konfiguracja środowiska pracy



### Selenium & Historia

![Selenium](img/selenium.png)



### Pierwszy test



### Document object model

* Document object model - reprezentacja dokumentów HTML i XHMTL w postaci obiektówej drzewiastej
* Model jest niezależny od platformy i języka programowania
* W3C DOM standaryzuje dostęp do tworzenia, usuwania i modyfikację tzw. węzłów w DOM
* Standard W3C definiuje interfejsy DOM tylko dla języków JavaScript i Java



### Znajdowanie elementów na stronie

* find_element_by_id
* find_element_by_name
* find_element_by_xpath
* find_element_by_link_text
* find_element_by_partial_link_text
* find_element_by_tag_name
* find_element_by_class_name
* find_element_by_css_selector


### Znajdowanie listy elementów na stronie

* find_elements_by_name
* find_elements_by_xpath
* find_elements_by_link_text
* find_elements_by_partial_link_text
* find_elements_by_tag_name
* find_elements_by_class_name
* find_elements_by_css_selector



### Element methods

* wszystkie metody do znajdowania elementów wewnątrz elementu
* clear
* click
* submit - dla każdego elementu form
* send_keys
* get_attribute
* get_property
* is_displayed
* is_enabled
* is_selected
* value_of_css_property


### Drivery

* Wszystkie drivery używają wspólnego protokołu komunikacyjnego JSON Wire Protocol

![Selenium](img/selenium2.png)


### Waity

* Implicit wait - mówi driverowi, żeby czekał określony czas kiedy próbuje znależć element w DOM

```python
from selenium import webdriver

driver = webdriver.Firefox()
driver.implicitly_wait(10) # seconds
driver.get("http://somedomain/url_that_delays_loading")
myDynamicElement = driver.find_element_by_id("myDynEle")
```



### Waity

* Explicit wait - czekanie przez określony czas na określony stan przed pójściem dalej
  * WebDriverWait
  * używa się expected conditions


### Expected conditions

* title_is
* title_contains
* presence_of_element_located
* visibility_of_element_located
* visibility_of
* presence_of_all_elements_located
* text_to_be_present_in_element
* text_to_be_present_in_element_value
* frame_to_be_available_and_switch_to_it
* invisibility_of_element_located
* element_to_be_clickable


* staleness_of
* element_to_be_selected
* element_located_to_be_selected
* element_selection_state_to_be
* element_located_selection_state_to_be
* alert_is_present



### Czekanie na elementy i ich statusy

```python
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

driver = webdriver.Firefox()
driver.get("http://somedomain/url_that_delays_loading")
try:
    element = WebDriverWait(driver, 10).until(
        EC.presence_of_element_located((By.ID,
        "myDynamicElement"))
    )
finally:
    driver.quit()
```



### Selecty

```python
from selenium.webdriver.support.ui import Select

driver = webdriver.Firefox()
driver.get("http://somedomain/")
select = Select(driver.find_element_by_name('name'))
select.select_by_index(index)
select.select_by_visible_text("text")
select.select_by_value(value)
```



### Driver nawigacja

```python
driver.switch_to.window("windowName")

for handle in driver.window_handles:
    driver.switch_to.window(handle)

driver.switch_to.frame("frameName")

driver.switch_to_default_content()

alert = driver.switch_to.alert()

driver.forward()
driver.back()
```



### Assercje w testach

* Assercja wyrażenie warunkówe które powinno być prawdziwe w określonym stanie, jeśli nie wykonanie zostaje przerwane
* Python ma wbudowany assert w język

```python
assert a > b , 'a is not bigger than b'
```

* Wg dokumentacji Pythona takie wyrażenie w trakcie wykonywania przekształcane jest na

```python
if __debug__:
    if not expression1:
        raise AssertionError(expression2)
```



### Zadanie



### Typy frameworków testowy

* TDD - xunit (unittest, pytest, nose)
* BDD - gherkin (behave, lettuce, cucumber)
* Keyword Driven Testing - Robot Framework



### Struktura testów unittest

* unittest od razu w pythonie (typowy xunit wzorowany na junit)
* Klasa z testami musi dziedziczyć z unittest.TestCase
  * setUp
  * test_case (musi mieć nazwę test na początku)
  * tearDown



### Wzorce projektowe

* Wzorzec projektowy (ang. design pattern) – uniwersalne, sprawdzone w praktyce rozwiązanie często pojawiających się, powtarzalnych problemów projektowych



### Page object pattern

![Selenium](img/pop.png)



### Base page (Page object pattern)

```python
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait

class Page:
    _driver: WebDriver

    def __init__(self, driver) -> None:
        self._driver = driver

    def find_element(self, by, locator):
        return self._driver.find_element(by, locator)
```



### Base page cnt (Page object pattern)

```python
    def find_elements(self, by, locator):
        return self._driver.find_elements(by, locator)

    def wait(self, timeout=30):
        return WebDriverWait(self._driver, timeout)
```



### One of the pages (Page object pattern)

```python
from selenium.webdriver.common.by import By

from pages.dashboard_page import DashboardPage
from pages.page import Page

class LoginPage(Page):

    txt_username = (By.ID, 'txtUsername')
    txt_password = (By.ID, 'txtPassword')
    btn_login = (By.ID, 'btnLogin')
```



### One of the pages cnt (Page object pattern)

```python
    def login(self, login, password):
        txt_uername_elm = self.find_element(*self.txt_username)
        txt_password_elm = self.find_element(*self.txt_password)
        btn_login_elm = self.find_element(*self.btn_login)
        txt_uername_elm.send_keys(login)
        txt_password_elm.send_keys(password)
        btn_login_elm.click()
        return DashboardPage()
```


### Test base

```python
import unittest

from selenium import webdriver

class TestBase(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.maximize_window()

    def tearDown(self):
        self.driver.close()
        self.driver.quit()
```



### Test

```python
from pages.login_page import LoginPage
from test_base import TestBase

class LoginPageTests(TestBase):

    def setUp(self):
        super().setUp()
        self.driver.get('https://opensource-demo.orangehrmlive.com/')
        self.login_page = LoginPage(self.driver)

    def test_login(self):
        dashboard_page = self.login_page.login('Admin',\
         'admin123')
        dashboard_page.assert_legends_schown()

```



### Architektura testów automatycznych unittest

* YAGNI
* SOLID
* KISS



### Zadania



### Lokatory - wspieranie przeciwdziałania zmianom

Gdy myślimy o lokatorze:

* attributes
* child
* descendant
* sibling
* order
* parent



### Zadania



### Wiele środowisk

```python
import configparser
from selenium import webdriver

config = configparser.ConfigParser()
config.read('config.ini')

driver_name: str = config['default']['driver']

if driver_name.lower() == 'firefox':
    driver = webdriver.Firefox()
elif driver_name.lower() == 'chrome':
    driver = webdriver.Chrome()
```



### Logowanie

```python
import logging

logging.debug('This is a debug message')
logging.info('This is an info message')
logging.warning('This is a warning message')
logging.error('This is an error message')
logging.critical('This is a critical message')
```



### Screenshoty

`driver.get_screenshot_as_file(file.png) `



### Raporty  

* HTMLTestRunner
* Allure
* Robot Framework
* Nose



### Łączenie z bazą danych

* Etapy
  * stworzenie połączenia
  * pobierz cursor
  * wykonaj działanie
  * jeśli było to działanie zmieniające dane w bazie wykonaj commit
  * zamknij cursor
  * zamknij połaczenie


### kod

```python
import sqlite3

mydb = sqlite3.connect('mydb.db')
mycursor = mydb.cursor()

table_create = '''CREATE TABLE customers (person_id INTEGER PRIMARY KEY,
name text NOT NULL,
surname text NOT NULL)'''

mycursor.execute(table_create)

val = ("John", "Green")
mycursor.execute(sql_insert, val)
mydb.commit()
print(mycursor.rowcount, "record inserted.")
mycursor.close()
mydb.close()
```


### Selenium Grid

![Selenium Grid](img/grid.png)



### Automatyzacja



### Warto zajrzeć

* <https://www.stavros.io/tutorials/python/> - Learn Python in 10 minutes
* <https://devhints.io/xpath> - Xpath vs css cheatsheet
* <https://pyformat.info/> - Python formatting
* <https://seleniumhq.github.io/selenium/docs/api/py/> - Selenium python api
